# Women authorship in Randomized control trial in Rheumatology: time evolution and influencing factors

This is the public repository containing the data and the code associated to the article "Women authorship in Randomized control trial in Rheumatology: time evolution and influencing factors" by Kim Lauper, Diana Buitrago Garcia, Delphine Courvoisier and Denis Mongin.

The file [analysis_tables.R](analysis_tables.R) conatins the R code to analyze the data and produce the results and tables. The file [utilds.R](utils.R) contains help functions to format the table and regression results.

The [full_dataset.csv](full_dataset.csv) file contains the data about the published RCT. The file has one line per author of each published RCT.
The variables are the following:

On the RCT publication level (these variables are repeated for each line of the author of the same publication)

- ID : Pubmed ID
- Year: year of publication
- Medline_date: Medline date
- Journal_title : journal name
- ImpactFactor : impact factor of the journal for the year of publication
- Condition_short : condition of the patients in the RCT
- Continent : continent of the RCT
- Sponsor : Is the sponsor public or private
- Intervention : Is the intervention of the RCT a drug or not
- Intervention_type : detail types of intervention
- ICMJE : does the journal support ICMJE recommandations
- Sample_Size : the planned sample size, in three categories
- Target_Sample_Size : the actual planned sample aize as described in the RCT registration

On the author level

- lastname : last name of the author
- forename: forename of the author
- author_pos : position of the author in teh author list
- Nauthor : number of authors for the publication
- private_affil : is the affiliations from a private pharmaceutilcal company
- country : country of affiliation
- gender : gender as proposed by the genderize API, based on forename and country
- probability : probability assiciated with gender determination